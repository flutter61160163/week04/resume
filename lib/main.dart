import 'package:flutter/material.dart';

void main() {
  runApp(MyResume());
}

class MyResume extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color, Icons.call, 'Tel.'),
          _buildButtonColumn(color, Icons.email, 'Email'),
          _buildButtonColumn(color, Icons.facebook, 'Facebook'),
        ],
      ),
    );
    Widget topContent = Container(
        padding: const EdgeInsets.all(32),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
                child: Image.asset(
              'images/CVPhoto.png',
              width: 200,
              height: 200,
            )),
            Expanded(
                child: Text(
              'Witthawat Meenuan',
              softWrap: true,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            )),
          ],
        ));
    Widget centerAge = Container(
        padding: const EdgeInsets.all(10),
        child: Row(mainAxisSize: MainAxisSize.min, children: [
          Image.asset(
            'images/age.png',
            width: 20,
            height: 20,
          ),
          Text(
            'Age',
            softWrap: true,
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black),
          ),
          Text(
            ' 22',
            softWrap: true,
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black),
          ),
        ]));
    Widget centerGender = Container(
        padding: const EdgeInsets.all(10),
        child: Row(mainAxisSize: MainAxisSize.min, children: [
          Image.asset(
            'images/gen.png',
            width: 20,
            height: 20,
          ),
          Text(
            'Gender',
            softWrap: true,
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black),
          ),
          Text(
            ' Male',
            softWrap: true,
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black),
          ),
        ]));
    Widget centerExp = Container(
        padding: const EdgeInsets.all(10),
        child: Row(mainAxisSize: MainAxisSize.min, children: [
          Image.asset(
            'images/exp.png',
            width: 20,
            height: 20,
          ),
          Text(
            'Experience',
            softWrap: true,
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black),
          ),
        ]));
    Widget centerExp1 = Container(
        padding: const EdgeInsets.all(10),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            '2019 - 2020',
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black),
          ),
          Text(
            'TIC-TAC-TOE GAME',
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black),
          ),
        ]));
    Widget centerExp2 = Container(
        padding: const EdgeInsets.all(10),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            '2020 - 2021',
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black),
          ),
          Text(
            'BUILD TANK GAME BY SIMPLE GAME JAVASCRIPT',
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black),
          ),
        ]));
    Widget centerExp3 = Container(
        padding: const EdgeInsets.all(10),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            '2020 - 2021',
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black),
          ),
          Text(
            'DESIGN UX&UI AND BUILD COFFEE SHOP PROGRAM',
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black),
          ),
        ]));
    Widget centerEducation = Container(
        padding: const EdgeInsets.all(10),
        child: Row(mainAxisSize: MainAxisSize.min, children: [
          Image.asset(
            'images/edu.png',
            width: 20,
            height: 20,
          ),
          Text(
            'Education',
            softWrap: true,
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black),
          ),
        ]));
    Widget centerEducation1 = Container(
        padding: const EdgeInsets.all(10),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            'MUEANG NAKHON NAYOK SCHOOL, NAKHON NAYOK, THAILAND',
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black),
          ),
          Text(
            'GPA 3.00',
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black),
          ),
        ]));
    Widget centerEducation2 = Container(
        padding: const EdgeInsets.all(10),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            'BURAPHA UNIVERSITY, CHON BURI, THAILAND',
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black),
          ),
          Text(
            'GPA 3.05',
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black),
          ),
        ]));
    Widget centerSkills = Container(
        padding: const EdgeInsets.all(10),
        child: Row(mainAxisSize: MainAxisSize.min, children: [
          Image.asset(
            'images/development-skill.png',
            width: 20,
            height: 20,
          ),
          Text(
            'Skills',
            softWrap: true,
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black),
          ),
        ]));
    Widget centerSkills1 = Container(
        padding: const EdgeInsets.all(10),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            'Curiosity',
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black),
          ),
          Text(
            'Time Management',
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black),
          ),
          Text(
            'Documentation',
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black),
          ),
        ]));
    Widget centerProgaming = Container(
        padding: const EdgeInsets.all(10),
        child: Row(mainAxisSize: MainAxisSize.min, children: [
          Image.asset(
            'images/laptop-coding.png',
            width: 20,
            height: 20,
          ),
          Text(
            'Progaming Skills',
            softWrap: true,
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black),
          ),
        ]));
    Widget centerProgaming1 = Container(
        padding: const EdgeInsets.all(10),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            'JavaScript',
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black),
          ),
          Text(
            'Vue',
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black),
          ),
          Text(
            'HTML',
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black),
          ),
          Text(
            'Java',
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black),
          ),
          Text(
            'CSS',
            style: TextStyle(
                fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black),
          ),
        ]));
    Widget centerProIcon = Container(
        padding: const EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
                child: Image.asset(
              'images/js.png',
              width: 50,
              height: 50,
            )),
            Expanded(
                child: Image.asset(
              'images/vue.png',
              width: 50,
              height: 50,
            )),
            Expanded(
                child: Image.asset(
              'images/html.png',
              width: 50,
              height: 50,
            )),
            Expanded(
                child: Image.asset(
              'images/java.png',
              width: 50,
              height: 50,
            )),
            Expanded(
                child: Image.asset(
              'images/css.png',
              width: 50,
              height: 50,
            )),
          ],
        ));
    Widget centerAddress = Container(
      padding: const EdgeInsets.all(10),
      child: Text(
        'Address : House No. 4/23 Village No.10Tha Chang Sub-district, Mueang Nakhon Nayok District, Nakhon Nayok Province Postal Code 26000',
        softWrap: true,
        style: TextStyle(
            fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black),
      ),
    );
    Widget centerTel = Container(
      padding: const EdgeInsets.all(10),
      child: Text(
        'Telephone : 0948567629',
        softWrap: true,
        style: TextStyle(
            fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black),
      ),
    );
    Widget centerEmail = Container(
      padding: const EdgeInsets.all(10),
      child: Text(
        'Email : Witthawat.Meenuan@gmail.com',
        softWrap: true,
        style: TextStyle(
            fontSize: 16, fontWeight: FontWeight.w400, color: Colors.black),
      ),
    );

    return MaterialApp(
        title: 'My Resume',
        home: Scaffold(
            appBar: AppBar(
              title: const Text('RESUME'),
            ),
            body: ListView(
              children: [
                topContent,
                centerAge,
                centerGender,
                centerExp,
                centerExp1,
                centerExp2,
                centerExp3,
                centerEducation,
                centerEducation1,
                centerEducation2,
                centerSkills,
                centerSkills1,
                centerProgaming,
                centerProgaming1,
                centerProIcon,
                centerAddress,
                centerTel,
                centerEmail,
                buttonSection
              ],
            )));
  }

  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Icon(icon, color: color),
        Container(
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ],
    );
  }
}
